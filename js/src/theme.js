( function( $ ) {
    var scotchPanel = $('#panel-example').scotchPanel({
        containerSelector: 'body',
        direction: 'right',
        duration: 300,
        transition: 'ease',
        clickSelector: '.toggle-panel',
        distanceX: '70%',
        enableEscapeKey: true
    });

    $(window).resize(function() {
        if ($(window).width() >= 768 && $('.scotch-panel-canvas').hasClass('scotch-is-showing')) {
            scotchPanel.close();
        }
    });

    $( document ).ready(function() {
        //Owl Carousel - Product Carousel
        var owlWrap = $('.homepage-banner');
        // checking if the dom element exists
        if (owlWrap.length > 0) {
            owlWrap.each(function(){
                var carousel= $(this).find('.slides');
                var arrows= $(this).find('.arrows');
                carousel.owlCarousel({
                    navRewind:true,
                    loop: false,
                    margin:0,
                    navContainer: arrows,
                    navText: ['<i class="fa fa-angle-left"></i>', '<i class="fa fa-angle-right"></i>'],
                    responsive:{
                        0:{
                            items:1,
                            nav:false
                        },
                        767:{
                            items:1,
                            nav:false
                        },
                        768:{
                            items:1,
                            nav:true
                        }
                    }
                })
            });
        }

    });
        
    $( document ).ready(function() {
        //Owl Carousel - Product Carousel
        var owlWrap = $('.portfolio-container');
        // checking if the dom element exists
        if (owlWrap.length > 0) {
            owlWrap.each(function(){
                var carousel= $(this).find('.portfolio');
                var arrows= $(this).find('.arrows');
                carousel.owlCarousel({
                    navRewind:true,
                    loop: false,
                    margin:30,
                    navContainer: arrows,
                    navText: ['<i class="fa fa-angle-left"></i>', '<i class="fa fa-angle-right"></i>'],
                    responsive:{
                        0:{
                            items:2,
                            nav:false
                        },
                        768:{
                            items:3,
                            nav:false
                        },
                        1000:{
                            items:4,
                            nav:true
                        }
                    }
                })
            });
        }

    });

    $( document ).ready(function() {
        //Owl Carousel - Product Carousel
        var owlWrap = $('.testimonial-container');
        // checking if the dom element exists
        if (owlWrap.length > 0) {
            owlWrap.each(function(){
                var carousel= $(this).find('.testimonial');
                var arrows= $(this).find('.arrows');
                carousel.owlCarousel({
                    navRewind:true,
                    loop: false,
                    margin:30,
                    dots: true,
                    responsive:{
                        0:{
                            items:1,
                            nav:false
                        }
                    }
                })
            });
        }

    });
    
    $( document ).ready(function() {
        $(window).resize(function(){
            // If there are multiple elements with the same class, "main"
            $('.portfolio-item').each(function() {
                $(this).height($(this).width());
            });

        }).resize();
    });
    
    $('p > img').unwrap();
    
    $( document ).ready(function() {
        $('.nav-sidebar').css({'display': 'block'});
    });

} )( jQuery );