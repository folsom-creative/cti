<?php
$background_color = '';
$background_color = get_sub_field('background_color'); //color picker
$background_image = '';
$background_image = get_sub_field('background_image'); //image
$title = '';
$title = get_sub_field('title');


// WP_Query arguments
$args = array (
	'post_type' => array( 'portfolio' ),
);
// The Query
$portfolios = new WP_Query( $args );
?>

<section class="portfolio-container" style="background-color: <?php echo $background_color; ?>; background-image: url(<?php echo $background_image['url']; ?>);">
	<div class="container">
		<div class="section-title"><?php echo $title; ?></div>
		<div class="divider"></div>
	</div>
	<div class="carousel-container container">
		<div class="arrows"></div>
		<div class="portfolio owl-carousel">
			<?php if ( $portfolios->have_posts() ): ?>
				<?php while ( $portfolios->have_posts() ): $portfolios->the_post(); ?>
					<?php
						$background = wp_get_attachment_url( get_post_thumbnail_id($post->ID) );
						$img_src = wp_get_attachment_image('full');
						$terms = get_the_terms( $post->ID , 'category' );
					?>
					<div class="portfolio-item" style="background-image: url(<?php echo $background; ?>);">
						<a class="overlay" href="<?php echo esc_url( get_permalink()); ?>">
							<div class="inner">
								<div class="title"><?php echo get_the_title(); ?></div>
								<div class="category"><?php foreach ( $terms as $term ) {echo  $term->name; echo '  ';} ?></div>
							</div>
						</a>
					</div>
				<?php endwhile; ?>
			<?php endif; ?>
		</div>
	</div>
</section>

<?php wp_reset_postdata(); ?>