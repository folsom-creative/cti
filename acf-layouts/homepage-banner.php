<?php



?>
<section class="homepage-banner">
	<?php if(have_rows('slide')): ?>
		<div class="arrows"></div>
		<div class="slides owl-carousel">
		
			<?php  while ( have_rows('slide') ) : the_row(); ?>
				<?php 
					//define the variables
					$title = '';
					$introduction = '';
					$button_text = '';
					$button_url = '';
					$content_placement = '';
					$background_image = '';

					//set the variables
					$title = get_sub_field('title');//text
					$introduction = get_sub_field('introduction');//textarea
					$button_text = get_sub_field('button_text');//text
					$button_url = get_sub_field('button_url');//url
					$content_placement = get_sub_field('content_placement');//radio button
					$background_image = get_sub_field('background_image');//image
				?>
				<?php if($content_placement == 'right'): ?>
					
					<div class="slide right" style="background-image: url(<?php echo $background_image['url']; ?>);">
						<div class="container">
							<div class="content">
								<div class="title"><?php echo $title; ?></div>
								<div class="divider"></div>
								<div class="introduction"><?php echo $introduction; ?></div>

								<a class="button-outline-white" href="<?php echo $button_url; ?>"><?php echo $button_text; ?></a>
							</div>
						</div>
					</div>
					
				<?php elseif($content_placement == 'left'): ?>
					
					<div class="slide left" style="background-image: url(<?php echo $background_image['url']; ?>);">
						<div class="container">
							<div class="content">
								<div class="title"><?php echo $title; ?></div>
								<div class="divider"></div>
								<div class="introduction"><?php echo $introduction; ?></div>

								<a class="button-outline-white" href="<?php echo $button_url; ?>"><?php echo $button_text; ?></a>
							</div>
						</div>
					</div>

				<?php elseif($content_placement == 'center'): ?>
					
					<div class="slide center" style="background-image: url(<?php echo $background_image['url']; ?>);">
						<div class="container">
							<div class="content">
								<div class="title"><?php echo $title; ?></div>
								<div class="divider"></div>
								<div class="introduction"><?php echo $introduction; ?></div>

								<a class="button-outline-white" href="<?php echo $button_url; ?>"><?php echo $button_text; ?></a>
							</div>
						</div>
					</div>
				
				<?php endif; ?>
			<?php endwhile; ?>
		</div>
	<?php endif; ?>
</section>