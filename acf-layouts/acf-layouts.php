<?php

// check if the flexible content field has rows of data
if( have_rows('module') ):

     // loop through the rows of data
    while ( have_rows('module') ) : the_row();

        if( get_row_layout() == 'content_editor' ):

        	get_template_part( 'acf-layouts/content_editor' );

        elseif( get_row_layout() == 'callout_banner' ):

        	get_template_part( 'acf-layouts/callout_banner' );

        elseif( get_row_layout() == 'callout_boxes' ):

        	get_template_part( 'acf-layouts/callout_boxes' );

        elseif( get_row_layout() == 'service_callout' ):

        	get_template_part( 'acf-layouts/service_callout' );

        elseif( get_row_layout() == 'packaging_feed' ):

        	get_template_part( 'acf-layouts/packaging_feed' );

        elseif( get_row_layout() == 'blog_feed' ):

        	get_template_part( 'acf-layouts/blog_feed' );

        elseif( get_row_layout() == 'testimonial_feed' ):

        	get_template_part( 'acf-layouts/testimonial_feed' );

        endif;

    endwhile;

else :

    // no layouts found

endif;

?>