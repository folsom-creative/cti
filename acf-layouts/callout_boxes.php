<?php
$background_color = '';
$background_color = get_sub_field('background_color'); //color picker
$background_image = '';
$background_image = get_sub_field('background_image'); //image
?>

<section class="calloutBoxes" style="background-color: <?php echo $background_color; ?>; background-image: url(<?php echo $background_image['url']; ?>);">
	<div class="container">
	<?php if( have_rows('box') ): ?>
		<?php while ( have_rows('box') ) : the_row(); ?>
			<?php
				$image = '';
				$title = '';
				$description = '';
				$link_text = '';
				$link_url = '';
				$image = get_sub_field('image'); //image
				$title = get_sub_field('title'); //text
				$description = get_sub_field('description'); //textarea
				$link_text = get_sub_field('link_text'); //text
				$link_url = get_sub_field('link_url'); //url
			?>
			<div class="col-4 mobile-full">
				<img class="image" src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" title="<?php echo $image['title']; ?>">
				<div class="title"><?php echo $title; ?></div>
				<div class="description"><?php echo $description; ?></div>
				<a class="link" href="<?php echo $link_url; ?>"><?php echo $link_text; ?></a>
			</div>
		<?php endwhile; ?>
	<?php endif; ?>
	</div>
</section>