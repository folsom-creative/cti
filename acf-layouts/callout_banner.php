<?php
$background_color = '';
$background_color = get_sub_field('background_color'); //color picker
$background_image = '';
$background_image = get_sub_field('background_image'); //image

$message = '';
$button_alpha = '';
$button_alpha_link = '';
$button_beta = '';
$button_beta_link = '';
$message = get_sub_field('message'); //text
$button_alpha = get_sub_field('button_alpha'); //text
$button_alpha_link = get_sub_field('button_alpha_link'); //url
$button_beta = get_sub_field('button_beta'); //text
$button_beta_link = get_sub_field('button_beta_link'); //url
?>

<section class="calloutBanner" style="background-color: <?php echo $background_color; ?>; background-image: url(<?php echo $background_image; ?>);">
	<div class="container">
		<div class="message"><?php echo $message; ?></div>
		<div class="buttons">
			<?php if($button_alpha): ?>
			<a class="button alpha" href="<?php echo $button_alpha_link; ?>"><?php echo $button_alpha; ?></a>
			<?php endif; ?>
			<?php if($button_beta): ?>
			<a class="button beta" href="<?php echo $button_beta_link; ?>"><?php echo $button_beta; ?></a>
			<?php endif; ?>
		</div>
	</div>
</section>