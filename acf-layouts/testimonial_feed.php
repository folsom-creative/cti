<?php
$background_color = '';
$background_color = get_sub_field('background_color'); //color picker
$background_image = '';
$background_image = get_sub_field('background_image'); //image

// WP_Query arguments
$args = array (
	'post_type' => array( 'testimonial' ),
);
// The Query
$testimonials = new WP_Query( $args );
?>

<section class="testimonial-container" style="background-color: <?php echo $background_color; ?>; background-image: url(<?php echo $background_image['url']; ?>);">
	<div class="container">
		<div class="section-title">TESTIMONIALS</div>
		<div class="quote-icon"><i class="fa fa-quote-left"></i></div>
	</div>
	<div class="carousel-container container">
		<div class="testimonial owl-carousel">
			<?php if ( $testimonials->have_posts() ): ?>
				<?php while ( $testimonials->have_posts() ): $testimonials->the_post(); ?>
					<?php
						$quote = '';
						$quote = get_field('quote'); //textarea
						$name = '';
						$name = get_field('name'); //text
						$company = '';
						$company = get_field('company'); //text
					?>
					<div class="testimonial-item" style="background-image: url(<?php echo $background; ?>);">
						<div class="inner">
							<div class="quote"><?php echo $quote; ?></div>
							<div class="name"><?php echo $name; ?></div>
							<div class="company"><?php echo $company; ?></div>
						</div>
					</div>
				<?php endwhile; ?>
			<?php endif; ?>
		</div>
	</div>
</section>

<?php wp_reset_postdata(); ?>