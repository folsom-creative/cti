<?php

$background_color = '';
$background_color = get_sub_field('background_color'); //color picker
$background_image = '';
$background_image = get_sub_field('background_image'); //image
$title = '';
$title = get_sub_field('title');
?>

<section class="blog-feed" style="background-color: <?php echo $background_color; ?>; background-image: url(<?php echo $background_image; ?>);">
	<div class="container">
	<div class="section-title"><?php echo $title; ?></div>
	<div class="divider"></div>
	<?php
		$args = array(
		    'posts_per_page' => 3,
		    'post_type' => 'post'
		    );
		// The Query
		$the_query = new WP_Query( $args );

		// The Loop
		if ( $the_query->have_posts() ) {
			
			while ( $the_query->have_posts() ) {
				echo '<div class="col-4 mobile-full blog-item">';
				$the_query->the_post();
				get_template_part( 'template-parts/post-item' );
				echo '</div>';
			}
			
		} else {
			// no posts found
		}
		/* Restore original Post Data */
		wp_reset_postdata();
	?>
	</div>
</section>