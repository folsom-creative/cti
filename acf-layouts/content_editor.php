<?php
$background_color = '';
$background_color = get_sub_field('background_color'); //color picker
$background_image = '';
$background_image = get_sub_field('background_image'); //image
$content = '';
$content = get_sub_field('content'); //wysiwyg


?>

<section class="content-editor" style="background-color: <?php echo $background_color; ?>; background-image: url(<?php echo $background_image['url']; ?>);">
	<div class="container">
		<?php echo $content; ?>	
	</div>
</section>