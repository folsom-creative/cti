<a href="<?php echo esc_url( get_permalink() ); ?>">
	<?php the_post_thumbnail(); ?>
</a>
<div class="inner">
	<a class="title" href="<?php echo esc_url( get_permalink() ); ?>"><?php the_title(); ?></a>
	<p class="excerpt"><?php echo get_excerpt(120); ?></p>
	<div class="dot-divider"></div>
	<span class="author"><strong>By : </strong><?php echo get_the_author(); ?></span><span> | </span><span><a class="readmore" href="<?php echo esc_url( get_permalink() ); ?>">Read more</a></span>
</div>