<?php
$banner_image = '';
$custom_title = '';
$sub_title = '';
$use_default_banner = '';

$banner_image = get_field('banner_image');
$custom_title = get_field('custom_title');
$sub_title = get_field('sub_title');
$use_default_banner = get_field('use_default_banner');

if($use_default_banner == true || $banner_image == ''){
	$banner_image = get_field('default_banner_image','option');;
}

if ( is_front_page() ) {

}
elseif(is_single()) {
	echo '<div class="title-banner single">';
	echo '</div>';
}
elseif(is_category()) {
	echo '<div class="title-banner category" style="background-image:url('.$banner_image['url'].');">';
		echo '<h1 class="title">';
			if($custom_title){
				echo $custom_title;
			}
			else {
				single_cat_title();
			}
		echo '</h1>';

		echo '<div class="sub-title">';
			echo $sub_title;
		echo '</div>';
	echo '</div>';
}
elseif(is_archive()) {
	echo '<div class="title-banner archive" style="background-image:url('.$banner_image['url'].');">';
		echo '<h1 class="title">';
			if($custom_title){
				echo $custom_title;
			}
			else {
				the_archive_title();
			}
		echo '</h1>';
		echo '<div class="sub-title">';
			echo $sub_title;
		echo '</div>';
	echo '</div>';
}
elseif(is_home() && !is_front_page()) {
	echo '<div class="title-banner blog" style="background-image:url('.$banner_image['url'].');">';
		echo '<h1 class="title">';
			if($custom_title){
				echo $custom_title;
			}
			else {
				single_post_title();
			}
		echo '</h1>';
		echo '<div class="sub-title">';
			echo $sub_title;
		echo '</div>';
	echo '</div>';
}
elseif(is_search()) {
	echo '<div class="title-banner search" style="background-image:url('.$banner_image['url'].');">';
		echo '<h1 class="title">';
			if($custom_title){
				echo $custom_title;
			}
			else {
				the_title();
			}
		echo '</h1>';
		echo '<div class="sub-title">';
			echo $sub_title;
		echo '</div>';
	echo '</div>';
}
elseif(is_404()) {
	echo '<div class="title-banner 404" style="background-image:url('.$banner_image['url'].');">';
		echo '<h1 class="title">';
			if($custom_title){
				echo $custom_title;
			}
			else {
				esc_html_e( 'Oops! That page can&rsquo;t be found.', 'cti' );
			}
		echo '</h1>';
		echo '<div class="sub-title">';
			echo $sub_title;
		echo '</div>';
	echo '</div>';
}
elseif(is_page()) {
	echo '<div class="title-banner page" style="background-image:url('.$banner_image['url'].');">';
		echo '<h1 class="title">';
			if($custom_title){
				echo $custom_title;
			}
			else {
				the_title();
			}
		echo '</h1>';
		echo '<div class="sub-title">';
			echo $sub_title;
		echo '</div>';
	echo '</div>';
}