<?php

// Register Custom Post Type
function portfolio_type() {

	$labels = array(
		'name'                  => _x( 'Portfolio Items', 'Post Type General Name', 'cti' ),
		'singular_name'         => _x( 'Portfolio', 'Post Type Singular Name', 'cti' ),
		'menu_name'             => __( 'Portfolio', 'cti' ),
		'name_admin_bar'        => __( 'Portfolio', 'cti' ),
		'archives'              => __( 'Portfolio Archives', 'cti' ),
		'parent_item_colon'     => __( 'Parent Portfolio:', 'cti' ),
		'all_items'             => __( 'All Portfolio Items', 'cti' ),
		'add_new_item'          => __( 'Add New Portfolio Item', 'cti' ),
		'add_new'               => __( 'Add New', 'cti' ),
		'new_item'              => __( 'New Item', 'cti' ),
		'edit_item'             => __( 'Edit Item', 'cti' ),
		'update_item'           => __( 'Update Item', 'cti' ),
		'view_item'             => __( 'View Item', 'cti' ),
		'search_items'          => __( 'Search Item', 'cti' ),
		'not_found'             => __( 'Not found', 'cti' ),
		'not_found_in_trash'    => __( 'Not found in Trash', 'cti' ),
		'featured_image'        => __( 'Featured Image', 'cti' ),
		'set_featured_image'    => __( 'Set featured image', 'cti' ),
		'remove_featured_image' => __( 'Remove featured image', 'cti' ),
		'use_featured_image'    => __( 'Use as featured image', 'cti' ),
		'insert_into_item'      => __( 'Insert into item', 'cti' ),
		'uploaded_to_this_item' => __( 'Uploaded to this item', 'cti' ),
		'items_list'            => __( 'Items list', 'cti' ),
		'items_list_navigation' => __( 'Items list navigation', 'cti' ),
		'filter_items_list'     => __( 'Filter items list', 'cti' ),
	);
	$args = array(
		'label'                 => __( 'Portfolio', 'cti' ),
		'description'           => __( 'Packaging Portfolio Items', 'cti' ),
		'labels'                => $labels,
		'supports'              => array( 'title', 'editor', 'thumbnail', ),
		'taxonomies'            => array( 'category' ),
		'hierarchical'          => false,
		'public'                => true,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'menu_position'         => 20,
		'menu_icon'             => 'dashicons-media-interactive',
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => true,
		'can_export'            => true,
		'has_archive'           => false,		
		'exclude_from_search'   => true,
		'publicly_queryable'    => true,
		'capability_type'       => 'page',
	);
	register_post_type( 'portfolio', $args );

}
add_action( 'init', 'portfolio_type', 0 );

// Register Custom Post Type
function testimonial_type() {

	$labels = array(
		'name'                  => _x( 'Testimonials', 'Post Type General Name', 'cti' ),
		'singular_name'         => _x( 'Testimonial', 'Post Type Singular Name', 'cti' ),
		'menu_name'             => __( 'Testimonials', 'cti' ),
		'name_admin_bar'        => __( 'Testimonials', 'cti' ),
		'archives'              => __( 'Testimonial Archives', 'cti' ),
		'parent_item_colon'     => __( 'Parent Testimonial:', 'cti' ),
		'all_items'             => __( 'All Testimonials', 'cti' ),
		'add_new_item'          => __( 'Add New Testimonial', 'cti' ),
		'add_new'               => __( 'Add New', 'cti' ),
		'new_item'              => __( 'New Item', 'cti' ),
		'edit_item'             => __( 'Edit Item', 'cti' ),
		'update_item'           => __( 'Update Item', 'cti' ),
		'view_item'             => __( 'View Item', 'cti' ),
		'search_items'          => __( 'Search Item', 'cti' ),
		'not_found'             => __( 'Not found', 'cti' ),
		'not_found_in_trash'    => __( 'Not found in Trash', 'cti' ),
		'featured_image'        => __( 'Featured Image', 'cti' ),
		'set_featured_image'    => __( 'Set featured image', 'cti' ),
		'remove_featured_image' => __( 'Remove featured image', 'cti' ),
		'use_featured_image'    => __( 'Use as featured image', 'cti' ),
		'insert_into_item'      => __( 'Insert into item', 'cti' ),
		'uploaded_to_this_item' => __( 'Uploaded to this item', 'cti' ),
		'items_list'            => __( 'Items list', 'cti' ),
		'items_list_navigation' => __( 'Items list navigation', 'cti' ),
		'filter_items_list'     => __( 'Filter items list', 'cti' ),
	);
	$args = array(
		'label'                 => __( 'Testimonial', 'cti' ),
		'description'           => __( 'Testimonials', 'cti' ),
		'labels'                => $labels,
		'supports'              => array( 'title', 'editor', 'thumbnail', ),
		'hierarchical'          => false,
		'public'                => true,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'menu_position'         => 20,
		'menu_icon'             => 'dashicons-format-quote',
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => true,
		'can_export'            => true,
		'has_archive'           => false,		
		'exclude_from_search'   => true,
		'publicly_queryable'    => true,
		'capability_type'       => 'page',
	);
	register_post_type( 'testimonial', $args );

}
add_action( 'init', 'testimonial_type', 0 );