<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package CTI
 */


$twitter = '';
$facebook = '';
$linkedin = '';
$youtube = '';

$twitter = get_field('twitter','option');
$facebook = get_field('facebook','option');
$linkedin = get_field('linkedin','option');
$youtube = get_field('youtube','option');

$enable_portfolio_widget = get_field('enable_portfolio_widget', 'option');

?>

	</div><!-- #content -->

	<footer id="colophon" class="site-footer" role="contentinfo">
		<div class="footer">
			<div class="container">
				<div class="col-3 mobile-full">
				<?php 
					if (  is_active_sidebar( 'footer-1' ) ) {
						dynamic_sidebar( 'footer-1' );
					}
				?>
				</div>
				<div class="col-3 mobile-full">
				<?php 
					if (  is_active_sidebar( 'footer-2' ) ) {
						dynamic_sidebar( 'footer-2' );
					}
				?>
				</div>
				<div class="col-3 mobile-full">
				<?php 
					if (  is_active_sidebar( 'footer-3' ) ) {
						dynamic_sidebar( 'footer-3' );
					}
					else {
						$args = array (
							'post_type' => array( 'portfolio' ),
						);
						// The Query
						$portfolios = new WP_Query( $args );
						?>
						<?php if($enable_portfolio_widget == true): ?>
						<section class="widget">
						<h3 class="widget-title">Recent Projects</h3>
						<?php if ( $portfolios->have_posts() ): ?>
							<?php while ( $portfolios->have_posts() ): $portfolios->the_post(); ?>
								<?php
									$background = wp_get_attachment_url( get_post_thumbnail_id($post->ID) );
									$img_src = wp_get_attachment_image('full');
									$terms = get_the_terms( $post->ID , 'category' );
								?>
								<div class="mini-portfolio col-3">
									<a class="link" href="<?php echo esc_url( get_permalink()); ?>">
										<img src="<?php echo $background; ?>">
									</a>
								</div>
							<?php endwhile; ?>
						<?php endif; ?>
						<?php endif; ?>
						</section>
						<?php
						wp_reset_postdata();
					}
				?>
				</div>
				<div class="col-3 mobile-full">
				<?php 
					if ( is_active_sidebar( 'footer-4' ) ) {
						dynamic_sidebar( 'footer-4' );
					}
				?>
				</div>
			</div>
		</div>
		<div class="site-info">
			<div class="container">
				<div class="copyright">&copy; <?php echo date('Y'); ?> <?php bloginfo( 'name' ); ?> All Rights Reserved.</div>
				<div class="right">
					<div class="social">
						<?php if($facebook): ?>
						<div class="twitter"><a href="<?php echo $twitter; ?>" target="_blank"><i class="fa fa-twitter"></i></a></div>
						<?php endif; ?>
						<?php if($twitter): ?>
						<div class="facebook"><a href="<?php echo $facebook; ?>" target="_blank"><i class="fa fa-facebook"></i></a></div>
						<?php endif; ?>
						<?php if($linkedin): ?>
						<div class="linkedin"><a href="<?php echo $linkedin; ?>" target="_blank"><i class="fa fa-linkedin"></i></a></div>
						<?php endif; ?>
						<?php if($youtube): ?>
						<div class="youtube"><a href="<?php echo $youtube; ?>" target="_blank"><i class="fa fa-youtube"></i></a></div>
						<?php endif; ?>
					</div>
				</div>
			</div>
		</div><!-- .site-info -->
	</footer><!-- #colophon -->
</div><!-- #page -->

<?php wp_footer(); ?>

</body>
</html>