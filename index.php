<?php
/**
 * The main template file.
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package CTI
 */

get_header(); ?>
<div class="container">
	<div id="primary" class="content-area">
		<main id="main" class="site-main blog-feed" role="main">

		<?php
		if ( have_posts() ) :

			if ( is_home() && ! is_front_page() ) : ?>
				<header>
					<h1 class="page-title screen-reader-text"><?php single_post_title(); ?></h1>
				</header>

			<?php
			endif;
			$count = 1;
			/* Start the Loop */
			while ( have_posts() ) : the_post();

				/*
				 * Include the Post-Format-specific template for the content.
				 * If you want to override this in a child theme, then include a file
				 * called content-___.php (where ___ is the Post Format name) and that will be used instead.
				 */
				//get_template_part( 'template-parts/content', get_post_format() );
				?>
				<div class="col-4 mobile-full blog-item <?php if ($count % 3 == 0) { echo 'last'; } ?>">
				<a href="<?php echo esc_url( get_permalink() ); ?>">
					<?php the_post_thumbnail(); ?>
				</a>
				<div class="inner">
					<a class="title" href="<?php echo esc_url( get_permalink() ); ?>"><?php the_title(); ?></a>
					<p class="excerpt"><?php echo get_excerpt(120); ?></p>
					<div class="dot-divider"></div>
					<span class="author"><strong>By : </strong><?php echo get_the_author(); ?></span><span> | </span><span><a class="readmore" href="<?php echo esc_url( get_permalink() ); ?>">Read more</a></span>
				</div>
				</div>
				<?php
				$count++;
			endwhile;

			the_posts_navigation();

		else :

			get_template_part( 'template-parts/content', 'none' );

		endif; ?>

		</main><!-- #main -->
	</div><!-- #primary -->
</div>
<?php get_footer(); ?>
