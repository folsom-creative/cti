<?php
/**
 * The template for displaying all single posts.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package CTI
 */

get_header(); ?>
<div class="container">
	<div id="primary" class="content-area col-8 mobile-full">
		<main id="main" class="site-main" role="main">
		<?php
		while ( have_posts() ) : the_post();

			?>
			<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
				<div class="col-12">
					<header class="entry-header">
						<?php
							if ( is_single() ) {
								the_title( '<h1 class="entry-title">', '</h1>' );
								$byline = sprintf(
									esc_html_x( 'By: %s', 'post author', 'cti' ),
									'<span class="author vcard"><a class="url fn n" href="' . esc_url( get_author_posts_url( get_the_author_meta( 'ID' ) ) ) . '">' . esc_html( get_the_author() ) . '</a></span>'
								);
								echo '<span class="byline"> ' . $byline . '  |  </span>';
								$categories_list = get_the_category_list( esc_html__( ', ', 'cti' ) );
								if ( $categories_list && cti_categorized_blog() ) {
									printf( '<span class="cat-links">' . esc_html__( 'Category: %1$s', 'cti' ) . '</span>', $categories_list ); // WPCS: XSS OK.
								}

							} else {
								
							}
					?>
					</header><!-- .entry-header -->
					<div class="entry-content">

						<?php
							the_content( sprintf(
								/* translators: %s: Name of current post. */
								wp_kses( __( 'Continue reading %s <span class="meta-nav">&rarr;</span>', 'cti' ), array( 'span' => array( 'class' => array() ) ) ),
								the_title( '<span class="screen-reader-text">"', '"</span>', false )
							) );
						?>
					</div><!-- .entry-content -->

					<footer class="entry-footer">
						<?php 
							// If comments are open or we have at least one comment, load up the comment template.
							if ( comments_open() || get_comments_number() ) :
								comments_template();
							endif;
							//cti_entry_footer(); 
						?>
					</footer><!-- .entry-footer -->
				</div>
			</article><!-- #post-## -->
			<?php

		endwhile; // End of the loop.
		?>

		</main><!-- #main -->
	</div><!-- #primary -->

<?php get_sidebar(); ?>
</div>
<?php get_footer(); ?>
