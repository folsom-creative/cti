<?php
/**
 * The header for our theme.
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package CTI
 */

?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="profile" href="http://gmpg.org/xfn/11">
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">

<?php wp_head(); ?>
</head>
<?php
$phone = '';
$email = '';
$hours = '';
$logo = '';
$tagline = '';
$twitter = '';
$facebook = '';
$linkedin = '';
$youtube = '';

$phone = get_field('phone','option');
$email = get_field('email','option');
$hours = get_field('hours','option');
$logo = get_field('logo','option');
$tagline = get_field('tagline','option');

$twitter = get_field('twitter','option');
$facebook = get_field('facebook','option');
$linkedin = get_field('linkedin','option');
$youtube = get_field('youtube','option');


?>
<body <?php body_class(); ?>>
<div id="page" class="site">
	<a class="skip-link screen-reader-text" href="#content"><?php esc_html_e( 'Skip to content', 'cti' ); ?></a>

	<header id="masthead" class="site-header" role="banner">
		<div class="toolbar">
			<div class="container">
				<div class="left">
					<div class="phone"><a href="tel:<?php echo $phone; ?>"><i class="fa fa-phone"></i> <?php echo $phone; ?></a></div>
					<div class="email"><a href="mailto:<?php echo $email; ?>?subject=Website Inquiry"><i class="fa fa-envelope-o"></i> <?php echo $email; ?></a></div>
					<div class="hours"><i class="fa fa-clock-o"></i> <?php echo $hours; ?></div>
				</div>
				<div class="right">
					<div class="social">
						<?php if($facebook): ?>
						<div class="twitter"><a href="<?php echo $twitter; ?>" target="_blank"><i class="fa fa-twitter"></i></a></div>
						<?php endif; ?>
						<?php if($twitter): ?>
						<div class="facebook"><a href="<?php echo $facebook; ?>" target="_blank"><i class="fa fa-facebook"></i></a></div>
						<?php endif; ?>
						<?php if($linkedin): ?>
						<div class="linkedin"><a href="<?php echo $linkedin; ?>" target="_blank"><i class="fa fa-linkedin"></i></a></div>
						<?php endif; ?>
						<?php if($youtube): ?>
						<div class="youtube"><a href="<?php echo $youtube; ?>" target="_blank"><i class="fa fa-youtube"></i></a></div>
						<?php endif; ?>
					</div>
				</div>
			</div>
		</div>
		<div class="navbar">
			<div class="container">
				<div class="site-branding ">
					<a href="<?php echo esc_url( home_url( '/' ) ); ?>"><img src="<?php echo $logo['url'] ?>" alt="<?php bloginfo( 'name' ); ?>" title="<?php bloginfo( 'name' ); ?>"></a>
					<span class="tagline"><?php echo $tagline; ?><br><?php echo $phone; ?></span>
				</div><!-- .site-branding -->

				<nav id="site-navigation" class="main-navigation " role="navigation">
					<?php wp_nav_menu( array( 'theme_location' => 'primary', 'menu_id' => 'primary-menu', 'container' => false, ) ); ?>
					<div class="search"><?php get_search_form(); ?></div>
				</nav><!-- #site-navigation -->

				<a href="#" class="toggle-panel"><i class="fa fa-bars"></i></a>
				<div id="panel-example" class="nav-sidebar" style="display:none;">
				    <?php wp_nav_menu( array( 'theme_location' => 'primary', 'menu_id' => 'primary-menu', 'container' => false, ) ); ?>
				</div>
			</div>
		</div>
	</header><!-- #masthead -->
	<?php
	if ( is_front_page() ) {
	    get_template_part('acf-layouts/homepage-banner');
	} else {
	}
	?>
	<?php get_template_part( 'template-parts/title-banner' ); ?>
	<div id="content" class="site-content">
